class cuenta :
    def __init__(self, t,c):
        self.titular = t
        self.cantidad = c
    def imprimir (self):
        print("titular: ", self.titular," cantidad: ", self.cantidad)

class CajaAhorro (cuenta):
    def __init__(self,x,c) :
        super().__init__(x,c)
    def imprimir(self) :
        super().imprimir()

class plazofijo (cuenta) :
    def __init__(self,t,c,p,i):
        super().__init__(t,c)
        self.plazo = p
        self.interes = i
    def importe_delinteres(self):
        r = self.cantidad * self.interes/100
        return r
    def imprimir(self):
        super().imprimir()
        print("\nplazo ", self.plazo, "interes: ", self.interes, "total de interes: ", self.importe_delinteres() )

caja1=CajaAhorro("Manuel",5000)
caja1.imprimir()
 
plazo1=plazofijo("Isabel",8000,365,1.2)
plazo1.imprimir()
