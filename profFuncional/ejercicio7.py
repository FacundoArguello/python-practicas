notas ={'Carmen':5, 'Antonio':4, 'Juan':8, 'Mónica':9, 'María': 6,
'Pablo':3}
def notas_ (s):
    if s > 5 :
        s= "aprobado"
    else:
        s="desaprovado"
    return s
notas = {nombre.upper():notas_(nota) for (nombre,nota) in notas.items()}
print(notas)