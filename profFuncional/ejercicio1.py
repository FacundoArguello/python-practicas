def apply_discount (precio, descuento):
    return precio - precio * descuento/100

def apply_IVA (precio, porcentaje):
    return precio + precio * porcentaje/100

def precio_cesta (d,function):
    total=0
    print(d.items())
    for pr,des in d.items():
        total+= function(pr,des)
        return total

print('El precio de la compra tras aplicar los descuentos es: ', precio_cesta({1000:20, 500:10, 100:1}, apply_discount))
print('El precio de la compra tras aplicar el IVA es: ', precio_cesta({1000:20, 500:10, 100:1}, apply_IVA))
