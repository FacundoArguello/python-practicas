inmueble =[{'año': 2000, 'metros': 100, 'habitaciones': 3, 'garaje': True, 'zona': 'A'},
{'año': 2012, 'metros': 60, 'habitaciones': 2, 'garaje': True, 'zona': 'B'},
{'año': 1980, 'metros': 120, 'habitaciones': 4, 'garaje': False, 'zona': 'A'},
{'año': 2005, 'metros': 75, 'habitaciones': 3, 'garaje': True, 'zona': 'B'},
{'año': 2015, 'metros': 90, 'habitaciones': 2, 'garaje': False, 'zona': 'A'}]

def añadir_precio(d):
    precio = (d["metros"] * 1000 + d["habitaciones"] * 5000 + d["garaje"] * 15000) * (1-(2020- d["año"])/100)

    if d["zona"] =="B":
        precio = (d["metros"] * 1000 + d["habitaciones"] * 5000 + d["garaje"] * 15000) * (1-(2020 - d["año"])/100)*1.5
    d["precio"]= precio

    return d

def comparar_precio(lista,precio) :
    l=[]
    l= [i for i in map(añadir_precio,lista) if i["precio"] < precio] 
    return l
    
print(comparar_precio(inmueble,100000))   
