n = int(input("Ingrese un numero del 1 al 10 : "))
file_name= "tabla-" + str(n)+".txt"
f= open(file_name,"w")

for i in range(1,11):
    f.write(str(i) + " x " + str(n) + " = " + str(n*i) + "\n")

f.close()