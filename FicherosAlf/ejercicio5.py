from urllib import request

f = "https://ec.europa.eu/eurostat/estat-navtree-portlet-prod/BulkDownloadListing?file=data/sdg_08_10.tsv.gz&unzip=true"

def read_url(url):
    try:
        file = request.urlopen(url)
    except:
        print("no existe")
    else:
        content= file.read().decode("utf-8").split("\n")
        content = [i.split("\t") for i in content]
        content= [list(map(str.strip, i)) for i in content]
        return content
print(read_url(f))