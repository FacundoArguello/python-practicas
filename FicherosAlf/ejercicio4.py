from urllib import request
from urllib.error import URLError

def read_url(url):
    try:
         f = request.urlopen(url)
    except :
        print("no existe el url")
    else:
        content = f.read()
        return len(content.split())
    
print(read_url("https://bitbucket.org/monkeyDfacu/parcial2odc/src/master/README.md"))
print(read_url("noex.txt"))


